import Store from './utils/Store';

const store = new Store({
	clients: [],
	isModalOpen: false
});

window.store = store;

export default store;
