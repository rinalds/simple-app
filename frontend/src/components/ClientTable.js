import { isEqual } from '../utils/general';

export const ClientTableEvents = {
	DELETE_CLIENT: 'DELETE_CLIENT',
	OPEN_CLIENT: 'OPEN_CLIENT'
};

class ClientTable extends HTMLElement {
	eventListeners = {};

	constructor() {
		super();
		this._clients = [];
	}

	on = (event, callback) => {
		this.eventListeners[event] = callback;
	};

	rowTemplate = client => `
		<tr>
			<td>
				<router-link href="/clients/${client.id}">${client.firstName} ${
		client.lastName
	}</router-link>
			<span class="mobile-only">${client.occupation}</span>
			</td>
			<td>${client.occupation}</td>
			<td>${client.age}</td>
			<td>${client.spentAmount.toFixed(2)}</td>
			<td>
				<button class='button button--small button--red delete-client'>Delete</button>
			</td>
		</tr>
	`;

	tableTemplate = () => `
		<div class="client-table">
			<div class="client-table__options">
				<button class="button add-client-button">Add Client</button>
			</div>
			<div class="client-table__container">
				<div class="spinner"></div>
				<table class="client-table__table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Occupation</th>
							<th>Age</th>
							<th>Spent Amount</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						${this.renderRows()}
					</tbody>
				</table>
			</div>
		</div>
	`;

	setClients = newClients => {
		const oldClients = this._clients;

		newClients.forEach((client, index) => {
			const oldClient = oldClients.find(
				oldClient => oldClient.id === client.id
			);

			if (!oldClient) {
				this.insertRow(index, client);
				if (client.new) {
					this._tableContainer.scrollTo(0, 7000);
				}
			} else if (!isEqual(client, oldClient)) {
				console.log('client', client);
				this.updateRow(client);
			}
		});

		oldClients.forEach(client => {
			if (!newClients.find(newClient => newClient.id === client.id)) {
				this.removeRow(client.id);
			}
		});

		this._clients = newClients;
	};

	renderRows = () => {
		return this._clients.map(this.rowTemplate).join('');
	};

	toggleLoading = loading => {
		this._tableContainer.className =
			'client-table__container' +
			(loading ? ' client-table__container--loading' : '');
	};

	insertRow = (index, client) => {
		const row = this._tableBody.insertRow(index);
		row.setAttribute('key', client.id);
		row.className = client.new ? 'new-client' : '';

		row.innerHTML = this.rowTemplate(client);
		row.querySelector('.delete-client').addEventListener(
			'click',
			this.deleteClient
		);
	};

	updateRow = client => {
		const row = this._tableBody.querySelector(`tr[key="${client.id}"]`);
		row.innerHTML = this.rowTemplate(client);
		row.querySelector('.delete-client').addEventListener(
			'click',
			this.deleteClient
		);
	};

	removeRow = id => {
		this.querySelector(`tr[key="${id}"]`).remove();
	};

	openClient = id => () => {
		if (this.eventListeners[ClientTableEvents.OPEN_CLIENT]) {
			this.eventListeners[ClientTableEvents.OPEN_CLIENT](id);
		}
	};

	deleteClient = e => {
		const rowEl = e.target.parentNode.parentNode;
		const key = rowEl.getAttribute('key');
		if (this.eventListeners[ClientTableEvents.DELETE_CLIENT]) {
			this.eventListeners[ClientTableEvents.DELETE_CLIENT](key);
		}
	};

	connectedCallback() {
		this.innerHTML = this.tableTemplate();

		this._tableContainer = this.querySelector('.client-table__container');
		this._tableBody = this.querySelector('tbody');
		this._addClientButton = document.querySelector('.add-client-button');

		this._addClientButton.addEventListener('click', this.openClient('new'));

		this.dispatchEvent(new Event('ready'));
	}
}

export default ClientTable;
