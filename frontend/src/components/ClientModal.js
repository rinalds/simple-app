import Modal from './Modal';

export const ClientModalEvents = {
	SAVE_CLIENT: 'SAVE_CLIENT'
};

class ClientModal extends Modal {
	connectedCallback() {
		super.connectedCallback();
		this._content.innerHTML = this.initialTemplate();

		this.dispatchEvent(new Event('ready'));
	}

	contentTemplate = client => `
		<form class="form">
			<div class="form__inputs">
				<label>First name</label>
				<input value="${client.firstName}" class="first-name-input">
				<label>Last name</label>
				<input value="${client.lastName}" class="last-name-input">
				<label>Occupation</label>
				<input value="${client.occupation}" class="occupation-input">
				<label>Age</label>
				<input value="${client.age}" class="age-input">
				<label>Amount spent</label>
				<input value="${client.spentAmount}" class="spent-amount-input">
			</div>
			<div class="form__buttons">
				<p class="form__error">Error in your form, fix it and save again</p>
				<button type="submit" class="button form__submit-button">Save</button>
			</div>
		</form>
	`;

	initialTemplate = () => `
		<div>
			<div class="spinner"></div>
		</div>
	`;

	showSpinner = () => {
		this._content.innerHTML = this.initialTemplate();
	};

	showLoadingButton = () => {
		const button = this._content.querySelector('.form__submit-button');

		button.innerHTML = 'Saving...';
		button.setAttribute('disabled', 'disabled');
	};

	showError = () => {
		this._content.querySelector('.form__error').className =
			'form__error form__error--show';
	};

	hideError = () => {
		this._content.querySelector('.form__error').className = 'form__error';
	};

	setClient = client => {
		this._client = client;
		this.setTitle(
			client.id ? `${client.firstName} ${client.lastName}` : 'New Client'
		);
		this._content.innerHTML = this.contentTemplate(this._client);
		this._content
			.querySelector('.form')
			.addEventListener('submit', this.onSubmit);
	};

	onSubmit = async e => {
		e.preventDefault();
		const firstName = this._content.querySelector('.first-name-input')
			.value;
		const lastName = this._content.querySelector('.last-name-input').value;
		const occupation = this._content.querySelector('.occupation-input')
			.value;
		const age = Number.parseInt(
			this._content.querySelector('.age-input').value,
			10
		);
		const spentAmount = Number.parseFloat(
			this._content.querySelector('.spent-amount-input').value
		);

		let updatedClient = {
			...this._client,
			firstName,
			lastName,
			occupation,
			age,
			spentAmount
		};

		if (this.eventListeners[ClientModalEvents.SAVE_CLIENT]) {
			this.eventListeners[ClientModalEvents.SAVE_CLIENT](updatedClient);
		}
	};
}

export default ClientModal;
