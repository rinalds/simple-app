import ScrollUtils from '../utils/ScrollUtils';

export const ModalEvents = {
	ON_CLOSE: 'ON_CLOSE'
};

class Modal extends HTMLElement {
	eventListeners = {};

	on = (event, callback) => {
		this.eventListeners[event] = callback;
	};

	open = () => {
		this._container.className = 'modal__container modal__container--open';
		document.addEventListener('keydown', this.onEscape);
		this._container.addEventListener('click', this.onClose);
		ScrollUtils.disableScroll();
	};

	close = () => {
		this._container.className = 'modal__container';
		document.removeEventListener('keydown', this.onEscape);
		this._container.removeEventListener('click', this.onClose);
		ScrollUtils.enableScroll();
	};

	setTitle = title => {
		this._title.innerHTML = title ? title : '&nbsp';
	};

	onEscape = e => {
		if (e.key === 'Escape') {
			if (this.eventListeners[ModalEvents.ON_CLOSE]) {
				this.eventListeners[ModalEvents.ON_CLOSE](this);
			}
		}
	};

	onClose = e => {
		if (e.target === e.currentTarget) {
			if (this.eventListeners[ModalEvents.ON_CLOSE]) {
				this.eventListeners[ModalEvents.ON_CLOSE](this);
			}
		}
	};

	connectedCallback() {
		this._container = document.createElement('div');
		this._container.className = 'modal__container';

		this._container.innerHTML = `
			<div class="modal">
				<div class="modal__options">
					<h2 class="modal__title">&nbsp</h2>
					<button>&#10005;</button>
				</div>
				<div class="modal__content"></div>
			</div>
		`;

		this.appendChild(this._container);

		this._content = this._container.querySelector('.modal__content');
		this._title = this._container.querySelector('.modal__title');

		this._container
			.querySelector('.modal__options button')
			.addEventListener('click', this.onClose);
	}
}

export default Modal;
