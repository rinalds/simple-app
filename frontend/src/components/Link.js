import Router from '../utils/Router';

class Link extends HTMLElement {
	_href = null;

	get href() {
		return this._href;
	}

	set href(href) {
		this._href = href;
	}

	onClick = e => {
		e.preventDefault();
		Router.goTo(this.href);
	};

	connectedCallback() {
		this.href = this.getAttribute('href');
		this.innerHTML = `<a class="link" href=${this.href}>${this.innerHTML}</a>`;
		this.querySelector('a').addEventListener('click', this.onClick);
	}
}

export default Link;
