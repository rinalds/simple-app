import store from '../store';
import Api from '../utils/Api';
import Router from '../utils/Router';

import { ClientTableEvents } from '../components/ClientTable';

class ClientTableContainer extends HTMLElement {
	connectedCallback() {
		// Won't be available immediatelly

		this._clientTable = document.createElement('client-table');
		this.appendChild(this._clientTable);

		this._clientTable.addEventListener('ready', this.init);
	}

	init = async () => {
		this._clientTable.removeEventListener('ready', this.init);

		store.on('clients', this.onClients);

		this._clientTable.on(
			ClientTableEvents.DELETE_CLIENT,
			this.onClientDelete
		);
		this._clientTable.on(ClientTableEvents.OPEN_CLIENT, this.onClientOpen);

		this._clientTable.toggleLoading(true);
		const clients = await Api.fetchClients();
		store.setState('clients', clients);
		this._clientTable.toggleLoading(false);
	};

	onClients = newClients => {
		this._clientTable.setClients(newClients);
	};

	onClientOpen = id => Router.goTo(`/clients/${id}`);

	onClientDelete = id => {
		Api.deleteClient(id);
		store.setState(
			'clients',
			store.state.clients.filter(value => value.id != id)
		);
	};
}

export default ClientTableContainer;
