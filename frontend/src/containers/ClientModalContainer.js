import store from '../store';
import Api from '../utils/Api';
import Router from '../utils/Router';

import { ClientModalEvents } from '../components/ClientModal';
import { ModalEvents } from '../components/Modal';

class ClientModalContainer extends HTMLElement {
	_loading = false;

	connectedCallback() {
		this._clientModal = document.createElement('client-modal');
		this.appendChild(this._clientModal);
		this._clientModal.addEventListener('ready', this.init);
	}

	init = async () => {
		this._clientModal.on(ClientModalEvents.SAVE_CLIENT, this.onSaveClient);

		this._clientModal.on(ModalEvents.ON_CLOSE, () => Router.goTo('/'));

		store.on('isModalOpen', async open => {
			if (open) {
				this._clientModal.open();
				this.initClient();
			} else {
				this._clientModal.close();
			}
		});
	};

	initClient = async () => {
		this._clientModal.showSpinner();

		const regexResult = /\/clients\/([0-9]+)/.exec(
			window.location.pathname
		);

		let client = {
			firstName: '',
			lastName: '',
			occupation: '',
			age: '',
			spentAmount: 0
		};

		if (regexResult && regexResult[1]) {
			const id = Number.parseInt(regexResult[1], 10);
			client = await Api.fetchClient(id);
		}
		this._clientModal.setClient(client);
	};

	onSaveClient = async updatedClient => {
		if (this._loading) {
			return false;
		}

		const isClientValid = Object.keys(updatedClient).reduce(
			(isValid, key, index, arr) => {
				if (
					['firstName', 'lastName', 'occupation'].includes(key) &&
					!updatedClient[key]
				) {
					return false;
				}

				if (
					['age', 'spentAmount'].includes(key) &&
					Number.isNaN(updatedClient[key])
				) {
					return false;
				}

				return isValid;
			},
			true
		);

		if (!isClientValid) {
			this._clientModal.showError();
			return false;
		}

		this._clientModal.hideError();
		this._loading = true;

		this._clientModal.showLoadingButton();

		const newClients = [...store.state.clients];

		if (updatedClient.id) {
			await Api.updateClient(updatedClient.id, updatedClient);

			const updatedClientIndex = store.state.clients.findIndex(
				client => client.id === updatedClient.id
			);

			newClients[updatedClientIndex] = updatedClient;
		} else {
			updatedClient = await Api.createClient(updatedClient);
			updatedClient.new = true;
			newClients.push(updatedClient);
		}

		store.setState('clients', newClients);

		this._loading = false;

		Router.goTo('/');
	};
}

export default ClientModalContainer;
