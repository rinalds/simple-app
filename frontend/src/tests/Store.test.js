import Store from '../utils/Store';

describe('Store', () => {
	it('should store state and on state change emit events', () => {
		expect.assertions(1);

		const store = new Store({
			clients: []
		});

		store.on('clients', clients => {
			expect(clients).toHaveLength(1);
		});

		store.setState('clients', ['new client']);
	});
});
