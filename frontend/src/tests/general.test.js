import { isEqual } from '../utils/general';

const case_1_same = {
	name: 'John',
	age: 23
};

const case_1 = {
	name: 'John',
	age: 23
};

const case_2 = {
	name: 'John',
	age: '23'
};
const case_3 = {
	name: 'John John',
	age: 23
};

const case_4 = {
	name: 'John',
	age: 23,
	ocuppation: 'Frontend'
};

describe('isEqual', () => {
	it('should compare 2 object properties if they are the same value and same number of properties', () => {
		expect(isEqual(case_1, case_1_same)).toEqual(true);

		expect(isEqual(case_1, case_2)).toEqual(false);
		expect(isEqual(case_1, case_3)).toEqual(false);
		expect(isEqual(case_1, case_4)).toEqual(false);
	});
});
