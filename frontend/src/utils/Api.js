const BASE_URL = '/api/v1';

const isDev = true;

const delay = ms => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve('done');
		}, ms);
	});
};

export default {
	fetchClients: async () => {
		isDev && (await delay(2000));

		const result = await fetch(`${BASE_URL}/clients`);
		if (result.status === 200) {
			const data = await result.json();
			return data.clients;
		}
		throw new Error('Something went wrong with api call');
	},

	fetchClient: async id => {
		isDev && (await delay(2000));

		const result = await fetch(`${BASE_URL}/clients/${id}`);
		if (result.status === 200) {
			const data = await result.json();
			return data.client;
		}
		throw new Error('Something went wrong with api call');
	},

	updateClient: async (id, data) => {
		isDev && (await delay(2000));

		const result = await fetch(`${BASE_URL}/clients/${id}`, {
			method: 'PUT', // *GET, POST, PUT, DELETE, etc.
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		});

		if (result.status === 200) {
			const data = await result.json();
			return data.client;
		}
		throw new Error('Something went wrong with api call');
	},

	createClient: async data => {
		isDev && (await delay(2000));

		const result = await fetch(`${BASE_URL}/clients`, {
			method: 'POST', // *GET, POST, PUT, DELETE, etc.
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		});

		if (result.status === 201) {
			const data = await result.json();
			return data.client;
		}
		throw new Error('Something went wrong with api call');
	},

	deleteClient: async id => {
		isDev && (await delay(2000));

		const result = await fetch(`${BASE_URL}/clients/${id}`, {
			method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				'Content-Type': 'application/json'
			}
		});

		if (result.status === 204) {
			return true;
		}
		throw new Error('Something went wrong with api call');
	}
};
