import store from '../store';

class Router {
	constructor() {
		this._interval = null;
	}

	static goTo(url) {
		window.history.pushState(
			{ prevUrl: window.location.pathname },
			null,
			url
		);
	}

	start = () => {
		this._interval = setInterval(this.checkIfRouteChanges, 50);
	};

	stop = () => {
		clearInterval(this._interval);
	};

	checkIfRouteChanges = () => {
		const {
			location: { pathname },
			history
		} = window;
		const { isModalOpen } = store.state;

		if (pathname.startsWith('/clients') && !isModalOpen) {
			store.setState('isModalOpen', true);
		} else if (
			!pathname.startsWith('/clients') &&
			history.state &&
			history.state.prevUrl.startsWith('/clients') &&
			isModalOpen
		) {
			store.setState('isModalOpen', false);
		}
	};
}

export default Router;
