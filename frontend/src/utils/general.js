export const isEqual = (obj1, obj2) => {
	const obj1Keys = Object.keys(obj1);
	const obj2Keys = Object.keys(obj2);
	if (obj1Keys.length !== obj2Keys.length) {
		return false;
	}

	return !obj1Keys.some(key => obj1[key] !== obj2[key]);
};
