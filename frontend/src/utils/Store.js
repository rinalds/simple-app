class Store {
	constructor(state) {
		this.state = { ...state };
		this.events = {};
	}

	on = (event, callback) => {
		if (!this.events[event]) {
			this.events[event] = [];
		}
		this.events[event].push(callback);
	};

	setState = (key, newValue) => {
		const prevValue = this.state[key];
		this.state[key] = newValue;

		if (this.events[key]) {
			this.events[key].forEach(func => {
				func(newValue, prevValue);
			});
		}
	};
}

export default Store;
