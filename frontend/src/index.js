import 'regenerator-runtime/runtime.js';

import ClientTableContainer from './containers/ClientTableContainer';
import ClientModalContainer from './containers/ClientModalContainer';

import ClientTable from './components/ClientTable';
import ClientModal from './components/ClientModal';
import Link from './components/Link';
import Router from './utils/Router';

const router = new Router();
router.start();

customElements.define('client-table-container', ClientTableContainer);
customElements.define('client-modal-container', ClientModalContainer);

customElements.define('client-table', ClientTable);
customElements.define('client-modal', ClientModal);
customElements.define('router-link', Link);
