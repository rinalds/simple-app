class Router {
	constructor() {
		this.routes = {};
	}

	use(url, method, req, res) {
		if (method === 'POST' || method === 'PUT') {
			let body = [];

			req.on('data', chunk => {
				body.push(chunk);
			}).on('end', () => {
				body = Buffer.concat(body).toString();
				body = JSON.parse(body);
				req.body = body;

				this.call(url, method, req, res);
			});
		} else {
			this.call(url, method, req, res);
		}
	}

	call(url, method, req, res) {
		const requestStr = `${method} ${url}`;

		const route = Object.keys(this.routes).find(regExRoute => {
			const regExp = new RegExp(regExRoute);
			return regExp.test(requestStr);
		});

		if (route) {
			const params = this._parseParams(
				requestStr,
				this.routes[route].paramsKeys,
				route
			);

			req.params = params;
			return this.routes[route].callback(req, res);
		}
		this._send404(req, res);
	}

	get(url, callback) {
		const { regExStr, paramsKeys } = this._parseUrl(url, 'GET');
		this.routes[regExStr] = { callback, paramsKeys };
	}

	post(url, callback) {
		const { regExStr, paramsKeys } = this._parseUrl(url, 'POST');
		this.routes[regExStr] = { callback, paramsKeys };
	}

	put(url, callback) {
		const { regExStr, paramsKeys } = this._parseUrl(url, 'PUT');
		this.routes[regExStr] = { callback, paramsKeys };
	}

	delete(url, callback) {
		const { regExStr, paramsKeys } = this._parseUrl(url, 'DELETE');
		this.routes[regExStr] = { callback, paramsKeys };
	}

	_parseUrl(url, method) {
		const paramsKeys = [...url.matchAll(/(:[A-z]+)/g)].map(
			param => param[0]
		);

		const regexedUrl = url
			.replace(/\//g, '\\' + '/')
			.replace(/:[A-z]+/g, '([A-z0-9]+)');

		let regExStr = `^${method} ${regexedUrl}$`;

		return { regExStr, paramsKeys };
	}

	_parseParams(url, paramKeys, routeRegExStr) {
		const params = {};

		const paramValues = [...url.match(new RegExp(routeRegExStr))].slice(1);

		paramKeys.forEach((key, index) => {
			params[key.replace(':', '')] = paramValues[index];
		});

		return params;
	}

	_send404(req, res) {
		res.writeHead(404, { 'Content-Type': 'application/json' });
		res.write(JSON.stringify({ error: 'No client found' }));
		res.end();
	}
}

export default Router;
