import Router from '../Router';
import clientModel from '../models/clientModel';

const router = new Router();

router.get('/', (req, res) => {
	res.writeHead(200, { 'Content-Type': 'application/json' });
	res.write(JSON.stringify({ clients: clientModel.findAll() }));
	res.end();
});

router.post('/', (req, res) => {
	const client = clientModel.create(req.body);

	res.writeHead(201, { 'Content-Type': 'application/json' });
	res.write(JSON.stringify({ client }));
	res.end();
});

router.get('/:id', (req, res) => {
	const id = Number.parseInt(req.params.id, 10);
	const client = clientModel.find(id);

	if (client) {
		res.writeHead(200, { 'Content-Type': 'application/json' });
		res.write(JSON.stringify({ client }));
		res.end();
	} else {
		res.writeHead(404, { 'Content-Type': 'application/json' });
		res.write(JSON.stringify({ error: 'No client found' }));
		res.end();
	}
});

router.put('/:id', (req, res) => {
	const id = Number.parseInt(req.params.id, 10);
	const client = clientModel.find(id);

	if (client) {
		const updatedClient = clientModel.update(id, req.body);
		res.writeHead(200, { 'Content-Type': 'application/json' });
		res.write(JSON.stringify({ client: updatedClient }));
		res.end();
	} else {
		res.writeHead(404, { 'Content-Type': 'application/json' });
		res.write(JSON.stringify({ error: 'No client found' }));
		res.end();
	}
});

router.delete('/:id', (req, res) => {
	const id = Number.parseInt(req.params.id, 10);

	if (clientModel.delete(id)) {
		res.writeHead(204);
		res.end();
	} else {
		res.writeHead(404, { 'Content-Type': 'application/json' });
		res.write(JSON.stringify({ error: 'No client found' }));
		res.end();
	}
});

export default router;
