import request from 'supertest';
import server from '../server';

const BASE_URL = '/api/v1';
const USER_ID = 201;

describe('GET /clients', () => {
	it('should return list of clients', async () => {
		const res = await request(server).get(`${BASE_URL}/clients`);

		expect(res.statusCode).toEqual(200);
		expect(res.body).toHaveProperty('clients');
		expect(res.body.clients).toHaveLength(200);
	});
});

describe('POST /clients', () => {
	it('should create new client', async () => {
		const userData = {
			firstName: 'John',
			lastName: 'Smith',
			occupation: 'Programmer',
			age: 25,
			spentAmount: 400
		};

		const res = await request(server)
			.post(`${BASE_URL}/clients`)
			.send(userData);

		expect(res.statusCode).toEqual(201);
		expect(res.body).toHaveProperty('client');
		expect(res.body.client).toEqual({ id: USER_ID, ...userData });
	});
});

describe('GET /clients/:id', () => {
	it('should return single client', async () => {
		const userData = {
			firstName: 'John',
			lastName: 'Smith',
			occupation: 'Programmer',
			age: 25,
			spentAmount: 400
		};

		const res = await request(server).get(`${BASE_URL}/clients/${USER_ID}`);

		expect(res.statusCode).toEqual(200);
		expect(res.body).toHaveProperty('client');
		expect(res.body.client).toEqual({ id: USER_ID, ...userData });
	});

	it('should return single 404 if client is not found', async () => {
		const res = await request(server).get('/clients/2000');

		expect(res.statusCode).toEqual(404);
	});
});

describe('PUT /clients/:id', () => {
	it('should return single client', async () => {
		const userData = {
			firstName: 'John',
			lastName: 'Smith',
			occupation: 'Programmer',
			age: 25,
			spentAmount: 400
		};

		const updateData = {
			spendAmount: 600,
			age: 26
		};

		const res = await request(server)
			.put(`${BASE_URL}/clients/${USER_ID}`)
			.send(updateData);

		expect(res.statusCode).toEqual(200);
		expect(res.body).toHaveProperty('client');
		expect(res.body.client).toEqual({
			id: USER_ID,
			...userData,
			...updateData
		});
	});
});

describe('DELETE /clients/:id', () => {
	it('should delete single client', async () => {
		const res = await request(server).delete(
			`${BASE_URL}/clients/${USER_ID}`
		);

		expect(res.statusCode).toEqual(204);
	});
});

describe('GET /clients/asd/asd', () => {
	it('should return 404', async () => {
		const res = await request(server).delete(`${BASE_URL}/clients/asd/asd`);

		expect(res.statusCode).toEqual(404);
	});
});
