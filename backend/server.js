import clientRoutes from './routes/clientRoutes';

import http from 'http';

const server = http.createServer((req, res) => {
	if (req.url.startsWith('/api/v1/clients')) {
		let url = req.url.replace('/api/v1/clients', '');
		url = url ? url : '/';
		clientRoutes.use(url, req.method, req, res);
	} else {
		res.writeHead(404);
		res.write('Page not Found');
		res.end();
	}
});

export default server;
