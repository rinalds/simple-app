import faker from 'faker';

const DB_SIZE = 200;

const db = new Map();
let lastID = DB_SIZE;

for (let id = 1; id <= DB_SIZE; id++) {
	db.set(id, {
		id,
		firstName: faker.name.firstName(),
		lastName: faker.name.lastName(),
		occupation: faker.name.jobTitle(),
		age: 18 + Math.round(Math.random() * 40),
		spentAmount: Number.parseFloat(faker.finance.amount())
	});
}

const clientModel = {
	findAll: () => {
		return [...db].map(entry => entry[1]);
	},
	create: data => {
		lastID += 1;
		const client = { id: lastID, ...data };
		db.set(lastID, client);
		return client;
	},
	find: id => {
		if (db.has(id)) {
			return db.get(id);
		}
		return null;
	},
	update: (id, data) => {
		if (!db.has(id)) {
			return null;
		}

		const updatedClient = { ...db.get(id), ...data };
		db.set(id, updatedClient);
		return updatedClient;
	},
	delete: id => {
		if (db.has(id)) {
			db.delete(id);
			return true;
		}
		return false;
	}
};

export default clientModel;
