# COMMANDS

-   npm install - Installs packages for both backend and frontend
-   npm run test-frontend - Runs frontend tests
-   npm run test-backend - Runs backend tests
-   npm start - Starts app
